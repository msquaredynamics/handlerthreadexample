package com.unox.handlerthreadexample

import android.os.*
import android.support.v7.app.AppCompatActivity
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.ref.WeakReference

val TAG = "MainActivity"

/**
 * This demo creates a HandlerThread that communicates with the UI thread using a dedicated Handler instance.
 * The HandlerThread exposes a function to post tasks into its internal queue. These tasks are executed in the
 * worker thread sequentially using the HandlerThread's MessageQueue.
 *
 * Run the program and click both the buttons one after the other rapidly:
 * 1) Notice that until the first task completes, the second one will not be executed.
 * 2) Notice that the tasks are always executed in the same Thread (workerThread), which is kept alive  by the Looper
 */
class MainActivity : AppCompatActivity() {

    private lateinit var imageDownloadHandlerThread: ImageResourceHandlerThread
    private lateinit var uiHandler: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        button_resize.setOnClickListener {
            imageDownloadHandlerThread.postTask(ImageResourceHandlerThread.RESIZE_RESOURCE, "myImage.png")
        }
        button_convert.setOnClickListener {
            imageDownloadHandlerThread.postTask(ImageResourceHandlerThread.CONVERT_RESOURCE, "myImage.png")
        }
    }

    override fun onResume() {
        /**
         * The uiHandler is bounded to the UI Thread and its MessageQueue. Therefore it can safely modify UI objects!
         */
        uiHandler = Handler {
            when (it.what) {
                ImageResourceHandlerThread.TASK_EXECUTING -> {
                    textview_status.append("Executing task: ${if (it.obj == ImageResourceHandlerThread.CONVERT_RESOURCE) "CONVERT RESOURCE" else "RESIZE RESOURCE"}... ")
                }

                ImageResourceHandlerThread.TASK_COMPLETED -> textview_status.append("Completed\n")
            }
            true
        }
        imageDownloadHandlerThread = ImageResourceHandlerThread("workerThread", uiHandler).apply {
            start()
        }

        super.onResume()
    }


    override fun onStop() {
        /**
         * When the activity stops, we also need to shutdown the ImageResourceHandlerThread instance, otherwise the
         * internal Looper will keep this thread alive forever. If the ImageResourceHandlerThread owns any strong
         * reference to any of this activity objects, then, the garbage collector will never be able to collect
         * the activity --> Memory Leak!
         */
        imageDownloadHandlerThread.quit()
        super.onStop()
    }
}



class ImageResourceHandlerThread(instanceName: String, _uiHandler: Handler) : HandlerThread(instanceName) {
    lateinit private var handler : Handler

    /** It is always a good idea to use WeakReferences when passing lifecycle objects to threads, because it helps
     * avoiding memory leaks. Using a WeakReference for the uiHandler allows the garbage collector to destroy the
     * reference to the Activity (owner of the uiHandler instance) even if the ImageResourceHandlerThread is still
     * alive.
     */
    private val uiHandler = WeakReference<Handler>(_uiHandler)

    override fun onLooperPrepared() {
        /**
         * Creates a Handler bounded to the worker thread and its Looper. Messages received here will therefore be
         * handled on the worker thread. *** We cannot modify UI objects from here! ***
         */
        handler = Handler(looper) {
            when (it.what) {
                RESIZE_RESOURCE -> {
                    // We are using a WeakReference, so we need to check if the reference was destroyed in the
                    // meanwhile...
                    uiHandler.get()?.obtainMessage(TASK_EXECUTING, RESIZE_RESOURCE)?.sendToTarget()
                    Thread.sleep(3000)
                    uiHandler.get()?.obtainMessage(TASK_COMPLETED, RESIZE_RESOURCE)?.sendToTarget()
                    Log.d(TAG, ": Task `Resize resource` completed - Thread: ${Thread.currentThread().name})")
                }

                CONVERT_RESOURCE -> {
                    uiHandler.get()?.obtainMessage(TASK_EXECUTING, CONVERT_RESOURCE)?.sendToTarget()
                    Thread.sleep(2000)
                    uiHandler.get()?.obtainMessage(TASK_COMPLETED, CONVERT_RESOURCE)?.sendToTarget()
                    Log.d(TAG, ": Task `Convert resource` completed (Thread: ${Thread.currentThread().name})")
                }
            }

            true
        }
    }

    /**
     * Insert a new task into the tasks queue
     */
    fun postTask(taskCode: Int, payload: Any) {
        handler.obtainMessage(taskCode, payload).sendToTarget()
    }



    companion object {
        const val RESIZE_RESOURCE = 1
        const val CONVERT_RESOURCE = 2
        const val TASK_EXECUTING = 3
        const val TASK_COMPLETED = 4
    }

}






